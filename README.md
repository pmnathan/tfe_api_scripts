# TFE API Workflow - Simplified Version

##Steps Overview

1. Get Workspace ID / Create Workspace and Retrieve Workspace ID
2. Create Workspace Variables (1 request per variable)
3. Create Configuration Version
4. List Existing/Pending Runs on the Workspace (Discard if needed)
5. Create Run
6. Inspect Plan/Policy and Check for approval
7. Perform an Apply
8. Get Status of Run





## Step 1. Get Workspace ID

```
curl  \
 --header "Authorization: Bearer $TOKEN"  \
 --header "Content-Type: application/vnd.api+json"   \
 https://app.terraform.io/api/v2/organizations/fanniemae_pov/workspaces/workspace-1 \
 | jq -r '.data.id'

```

## Step 1b. Create Workspace and Retrieve Workspace ID 
(if Setp 1 returns null)

```
create_workspace.json file contents
{
  "data": {
    "attributes": {
      "name": "workspace-1"
    },
    "type": "workspaces"
  }
}


curl   \
--header "Authorization: Bearer $TOKEN"   \
--header "Content-Type: application/vnd.api+json"  \
 --request POST   \
 --data @create_workspace.json  \
 https://app.terraform.io/api/v2/organizations/fanniemae_pov/workspaces


```

Workspace ID would look like - **ws-1mXPKu1hfjGSQ8cK**


## Step 2. Create Workspace Variables (1 request per variable)

```
variable_1.json file contents
{
    "data": {
      "type":"vars",
      "attributes": {
        "key":"instance_count",
        "value":"2",
        "description":"Instance Count",
        "category":"terraform",
        "hcl":false,
        "sensitive":false
      }
    }
  }

curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @variable_1.json \
  https://app.terraform.io/api/v2/workspaces/ws-bk3z9t33fLYeaDtR/vars


```
Setting up AWS Credentials - Note the following two variables are set as Environment Variables.

```
aws_access_key.json file contents
{
    "data": {
      "type":"vars",
      "attributes": {
        "key":"AWS_ACCESS_KEY",
        "value":"AK****************",
        "description":"AWS Access Key",
        "category":"env",
        "hcl":false,
        "sensitive":false
      }
    }
  }

curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @aws_access_key.json \
  https://app.terraform.io/api/v2/workspaces/ws-bk3z9t33fLYeaDtR/vars

```

```
aws_secret_key.json file contents
{
    "data": {
      "type":"vars",
      "attributes": {
        "key":"AWS_SECRET_KEY",
        "value":"AK****************",
        "description":"AWS Secret Key",
        "category":"env",
        "hcl":false,
        "sensitive":false
      }
    }
  }

curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @aws_secret_key.json \
  https://app.terraform.io/api/v2/workspaces/ws-bk3z9t33fLYeaDtR/vars

```

## Step 3. Create Configuration Version

Tar the TF file so that it can be uploaded into TFE.

```
tar -czf config.tar.gz -c aws_tf_config_sample/ .
```

```
create_config.json file contents

{
  "data": {
    "type": "configuration-versions",
    "attributes": {
      "auto-queue-runs": true
    }
  }
}



curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @create_config.json \
  https://app.terraform.io/api/v2/workspaces/ws-bk3z9t33fLYeaDtR/configuration-versions | jq
  
output contains Upload URL e.g
https://archivist.terraform.io/v1/object/dmF1bHQ6djE6Z0k3aTZ3UUpjSHFvSW00blRGcXlDQWxHQ20xRGRnYnVmVkp1QS9LVnlIVDZmQzNFWitUbEU2TktwcVFlbGxNRXBFZzNNYklubGc0L2tiTUVNem9LSE5qYTEwa2FjWWZxSUg0TS9KNzhpQ215T1VkaXIzaXlCNng0YlFRNFozMjZIK3Zsc2dhSks4RklRcGhvYm1HR1g4dnZrSWhUd2RQbWpJLzlaVC9DZE9kWHM1QVkzZ2s2N2w2RGFkQXQ3YlM1bWs5K05BcnNGc2xMQU9ZaHRkdlVIZ1VSTjUvWUcyUURNTjI3WlUwZDFDVXUzbDBlbVVEVHdrWFlTWWV1R0dMM1JqMThhWTNJTThSbi96VXlhTVlPR1Z6K0NpNitkWnM9
  
```

The Above request will return an Upload URL that will be used to upload the tarred file. We will also need the configuration Id to perform the Run.


Now we upload the file
```
curl \
  --header "Content-Type: application/octet-stream" \
  --request PUT \
  --data-binary @config.tar.gz \
https://archivist.terraform.io/v1/object/dmF1bHQ6djE6bVpaVDJ0SS9uNitUQk5ORjhla2NZUmhMNXZjK2p3d0xDVmlzcU5JS0tpdmNXc0t3ZXNrTHBsbnZhWmdybUpaS1h5eVRxQzIxUXF2VWhNa3YxenFBR2R5a1MvV1RaUlloL2JnWGlNRUYvSnlvK20va0EvZDFqOE9NbTBVRTQ5aVdVUTF6UEp5U0VYamRDR0FKU04rZHR3K2JzU3dXTHhRMDB5dXlONlp0dWNLRnNPTmZXWGF4WmJlSWVnWGxhb2NkbndSS0szYjFQdTA5cEJmMzNKUXkwc3NMdHg3bTNpRG1ScmVMWVlaZkh4cy93bkZBcGxKUEtLUk03SHdtbVJobWRyUEpKZkNjamFtelhocUNDeWhFMGdPYUUzcGtvN2s9
```


## Step 4. List Existing/Pending Runs on the Workspace 
If they need to be discarded

```
curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  https://app.terraform.io/api/v2/workspaces/ws-bk3z9t33fLYeaDtR/runs

```



## Step 5. Create Run
 

```

create_run.json file contents
{
    "data": {
      "attributes": {
        "is-destroy":false,
        "message": "Custom message"
      },
      "type":"runs",
      "relationships": {
        "workspace": {
          "data": {
            "type": "workspaces",
            "id": "ws-bk3z9t33fLYeaDtR"
          }
        },
        "configuration-version": {
          "data": {
            "type": "configuration-versions",
            "id": "cv-NmnKvFAwL56gxejy"
          }
        }
      }
    }
  }
  

curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @create_run.json \
  https://app.terraform.io/api/v2/runs
  
```

## Step 6. Inspect Plan/Policy Check for approval.
The URL for both Plan and Policy check is found in Step 5 output.

```
curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  https://app.terraform.io/api/v2/runs/run-4sLt56RKz6zLBwJf/plan



curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  https://app.terraform.io/api/v2/runs/run-7sbSSdyAcjB1ne9u/policy-checks



```

## Step 7. Perform an apply.

This should be done if the plan and policy check suceed.

```

curl \
  --header "Authorization: Bearer $TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request POST \
  --data @perform_apply.json \
  https://app.terraform.io/api/v2/runs/run-WYi5ScbQZDZPefkM/actions/apply
  
```

## Step 8. Get Status of Run




