terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "FMR_BU2"

    workspaces {
      name = "test_workspace"
    }
  }
}


provider "aws" {
  version = "~> 2.0"
  region  = var.region
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  count         = var.instance_count
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  tags = {
    Name = "terraform_workshop_-${count.index}"
    Environment = "development"
  }
}